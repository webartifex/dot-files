# General Aliases
alias bp='bpython'
alias bpython3='bpython'
alias h='htop'
alias j='pipenv run jupyter'
alias jn='pipenv run jupyter notebook'
alias p='python3'
alias pi='ipython'  # overwrites pi package
alias python='python3'
alias pr='pipenv run'
alias r='R'
alias sf='screenfetch'
alias ud='sudo ufw disable'
alias ue='sudo ufw enable'
alias v='pipenv shell'
alias vp='pipenv run python'


# Apt-get Aliases
alias agc='sudo apt-get autoclean'
alias agr='sudo apt-get autoremove'
alias agi='sudo apt-get install'
alias ago='sudo orphaner'
alias agu='sudo apt-get update'
alias aguu='sudo apt-get upgrade'
alias aguuu='sudo apt-get dist-upgrade'


# Git Aliases

# 2 letter versions
alias ga='git add'
alias gd='git diff'
alias gf='git fetch'
alias gl='git log'
alias gla='git log --all'
alias gp='git push'  # overwrites pari-gp
alias gs='git status'  # overwrites Ghostscript CLI
alias gs-='git status --'

# 3 letter versions
alias gad='git add'
alias gbr='git branch'
alias gbra='git branch -a'
alias gbrD='git branch -D'
alias gbrd='git branch -d'
alias gbrr='git branch -r'
alias gbrv='git branch -v'
alias gbrvv='git branch -vv'
alias gch='git checkout'
alias gch-='git checkout --'
alias gchb='git checkout -b'
alias gco='git commit'
alias gcoam='git commit --amend'
alias gcom='git commit -m'
alias gcoma='git commit -a -m'
alias gdi='git diff'
alias gfe='git fetch'
alias glo='git log'
alias gloa='git log --all'
alias gme='git merge'
alias gmeff='git merge --ff-only'
alias gol='git log --oneline --graph'  # overwrites growl-for-linux
alias gola='git log --oneline --graph --all'
alias gpu='git push'
alias gre='git rebase'  # note that "git reset" is gun
alias grei='git rebase -i'
alias gri='git rebase -i'  # overwrites gri
alias gst='git status'
alias gsh='git show'
alias gun='git reset'
alias gunh='git reset --hard'
alias gun-='git reset HEAD --'

# Disable "git pull" and "git add ."
git() {
    if [[ $@ == "pull" ]]; then
        printf "'git pull' is not allowed on this machine.\n";
        printf "Use 'git fetch' instead and then merge manually.\n";
    elif [ "$1" = "add" -o "$1" = "stage" ]; then
        if [ "$2" = "." ]; then
            printf "'git add .' is not allowed on this machine.\n";
            printf "Stage files explicitly instead.\n";
        else
            command git "$@";
        fi
    else
        command git "$@";
    fi;
}

